const fetch = require('./node_modules/node-fetch');
const { URLSearchParams } = require('url');
const commandLineArgs = require('command-line-args')
const commandLineUsage = require('command-line-usage')
const chalk = require('chalk');

if (process.argv.length < 5) {
  console.log("usage");
  const header =`Kill All Zombie Runners`;

   const sections = [
    {
      content: chalk.red(header),
      raw: true
    },
    {
      header: 'Synopsis',
      content: [
        '$ node index.js [-s <gitlab server url>] --e <email> -p <password>',
      ]
    }
  ]
  console.log(commandLineUsage(sections));
  process.exit();
}

const optionDefinitions = [
  { name: 'server', alias: 's', type: String, defaultValue: 'https://gitlab.com' },
  { name: 'email', alias: 'e', type: String },
  { name: 'password', alias: 'p', type: String }
];

const options = commandLineArgs(optionDefinitions)
run(options.server, options.email, options.password)
  .then(it => console.log("finished"))
  .catch(ex => console.error(ex));

async function getToken(base, username, password) {
  const params = new URLSearchParams();
  params.append('grant_type', 'password');
  params.append('username', username);
  params.append('password', password);

  const res = await fetch(`${base}/oauth/token`, { method: 'POST', body: params });
  checkStatus(res);
  const result = await res.json();
  return result.access_token;
}

async function listRunners(base, token, tags) {
  const res = await fetch(`${base}/api/v4/runners?status=offline&tag_list=${tags}`, { method: 'GET', headers: { 'Authorization': `Bearer ${token}` } });
  return await res.json();
}

async function deleteRunner(base, token, runner) {
  const res = await fetch(`${base}/api/v4/runners/${runner}`, { method: 'DELETE', headers: { 'Authorization': `Bearer ${token}` } });
  checkStatus(res);
}

function checkStatus(res) {
  if (res.ok) { // res.status >= 200 && res.status < 300
      return res;
  } else {
    console.error(res.statusText);
    throw new Error(res.statusText);
  }
}

async function run(base, email, password) {
  tags='script-maven'
  const token = await getToken(base, email, password);
  console.log('token', token)
  let runners = await listRunners(base, token, tags);
  while (runners.length > 0) {
    for (const runner of runners) {
      console.log("deleting ", runner);
      await deleteRunner(base, token, runner.id);
    }
    runners = await listRunners(base, token, tags);
  }
}
